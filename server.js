var express = require("express");
var mysql = require("mysql");
var bodyParser = require("body-parser");
var app = express();
var apiRoutes = express.Router();
app.use(bodyParser.json()); // for å tolke JSON
const PersonDao = require("./dao/persondao.js");

var pool = mysql.createPool({
  connectionLimit: 2,
  host: "mysql.stud.iie.ntnu.no",
  user: "janloe",
  password: "VwbsMtKm",
  database: "janloe",
  debug: false
});

let personDao = new PersonDao(pool);

// ---- get-metode for alle personer i tabell ----
app.get("/person", (req, res) => {
  console.log("/person: fikk request fra klient");
  personDao.getAll((status, data) => {
    res.status(status);
    res.json(data);
  });
});

// ---- get-metode for en person i tabell (på person-id) ----
app.get("/person/:personId", (req, res) => {
  console.log("/person/:personId: fikk request fra klient");
  personDao.getOne(req.params.personId, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

// ---- post-metode for å poste en person ----
app.post("/person", (req, res) => {
  console.log("Fikk POST-request fra klienten");
  personDao.createOne(req.body, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

// ---- put-metode for å oppdatere en person (på person-id) ----
app.put("/person/:personId", (req, res) => {
  console.log("/person/:personId: fikk request fra klient");
  personDao.updateOne(req.body, req.params.personId, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

// ---- delete-metode for å slette en person (på person-id) ----
app.delete("/person/personId:", (req, res) => {
  console.log("Fikk DELETE-request fra klienten");
  personDao.deleteOne(req.params.personId, (status, data) => {
    res.status(status);
    res.json(data);
  });
});
var server = app.listen(8080);
